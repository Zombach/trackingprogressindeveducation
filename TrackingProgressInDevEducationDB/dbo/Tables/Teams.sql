﻿CREATE TABLE [dbo].[Teams] (
    [id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TEAMS] PRIMARY KEY CLUSTERED ([id] ASC)
);

