﻿using TrackingProgressInDevEducationDAL.Models.Interface;

namespace TrackingProgressInDevEducationDAL.Models.Bases
{
    public class Homeworks : IModels
    {
        public string Name { get; set; }
    }
}