﻿using TrackingProgressInDevEducationDAL.Models.Interface;

namespace TrackingProgressInDevEducationDAL.Models.Bases
{
    public class CommentType : IModels
    {
        public string Name { get; set; }
    }
}